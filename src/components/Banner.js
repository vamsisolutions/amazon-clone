import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css"; 


export default function Banner() {
    return (
        <div className = "relative">
            <div className ="absolute w-full h-32 bg-gradient-to-t from-gray-100 to-transparent  bottom-0 z-20" />
            <Carousel
                autoPlay
                infiniteLoop
                showIndicators={false}
                showStatus={false}
                showThumbs={false}
                interval={5000}
            >
                <div>
                    <img loading="lazy" src="/images/slide1.jpg"  />
                </div>
                <div>
                    <img loading="lazy" src="/images/slide2.jpg"  />
                </div><div>
                    <img loading="lazy" src="/images/slide3.jpg"  />
                </div>

            </Carousel>
        </div>
    )
}
